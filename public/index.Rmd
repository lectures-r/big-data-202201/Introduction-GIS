---
pagetitle: "Introducción a GIS"
title: "\nBig Data and Machine Learning en el Mercado Inmobiliario\n"
subtitle: "\nLectura R 2: Introdución a GIS\n"
author: "\nEduard F. Martínez-González\n"
date: "Universidad de los Andes | [Edu-Continuada](https://ignaciomsarmiento.github.io/teaching/BDML)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---
```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(knitr,dplyr,tidyr,tibble,datasets,data.table)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

## Hoy veremos

### **1.** Datos geográficos

### **2.** Sistemas de Referencia de Coordenadas - CRS

### **3.** R-espacial

<!------------------------->
<!--- Datos geográficos --->
<!------------------------->

# [1.] Datos geográficos
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!---------------->
## ¿Qué es GIS?

Un **Sistema de Información Geográfico** (GIS por sus siglas en inglés), es un sistema (software + hardware) que permite gestionar y analizar la información capturada en el entorno físico.

<div><img src="pics/gis_intro.png" height=450></div>

Ver código de R [aquí](https://gitlab.com/Lectures-R/big-data-202201/Introduction-GIS/-/blob/main/public/clase_02/source/figure_intro_gis.R)

<!---------------->
## Modelos de datos geográficos
  
#### Datos vectoriales:

La geometría de este tipo de datos se compone de uno o más vértices interconectados. Un vértice describe una posición en el espacio utilizando un un eje X y un eje Y (opcionalmente también un eje z). Mas información [aquí](https://docs.qgis.org/2.14/es/docs/gentle_gis_introduction/vector_data.html).

#### Datos raster:

Un conjunto de datos ráster esta compuesto matriz de píxeles (también conocidos como celdas). Cada píxel representa una región geográfica, y el valor del píxel representa alguna característica de la región. Mas información [aquí](https://docs.qgis.org/2.14/es/docs/gentle_gis_introduction/raster_data.html).

<!---------------->
## Geometrías para datos vectoriales

<div><img src="pics/geometry.png" height=400></div>
Tomado de: [Applied Spatial Data Analysis with R](https://www.springer.com/gp/book/9781461476177)   

**Punto:** está compuesto de un solo vértice.
**Línea o arco:** está compuesto de dos o más vértices, pero el primer y el último vértice deben ser diferentes. **Polígono:** está compuesto de tres o más vértices, pero el último vértice es igual a la primero.

<!---------------->
## Extensiones de los archivos

| Nombre | Extensión	| Tipo | 
|:-|-:|-:|
| ESRI Shapefile	| .shp (archivo principal) |	Vector
| GeoJSON	|.geojson | Vector
| KML	| .kml | Vector
| GPX	| .gpx | Vector
| GeoTIFF	| .tif/.tiff | Raster
| Arc ASCII	| .asc | Raster
| R-raster |	.gri, .grd	| Raster
| SQLite/SpatiaLite	| .sqlite	| Vector y raster
| ESRI FileGDB	| .gdb	| Vector y raster
| GeoPackage	| .gpkg	| Vector y raster

Nota: Además del archivo **.shp** (que almacena la geometria del objeto), el ESRI Shapefile debe contener un archivo **.shx** (almacena el indice de la geometría), un archivo **.dbf** (dataframe con los atributos) y un archivo **.prj** (contiene información del CRS).

<!--------------------------------------------------->
<!--- Sistemas de Referencia de Coordenadas (CRS) --->
<!--------------------------------------------------->
# [2.] Sistemas de Referencia de Coordenadas - CRS
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!---------------->
## Sistemas de Coordenadas de Referencia (CRS)

Todo objeto espacial esta asociado a un CRS (Coordinate Reference Systems). El CRS proporciona una referencia espacial estandarizada de un objeto. Un CRS puedes ser **Sistema de Coordenadas Geográficas** o un **Sistema de Coordenadas Proyectadas**.

#### Sistema de Coordenadas Geográficas - SCG

En este SCG las ubicaciones se miden en unidades angulares desde el centro de la tierra en relación con dos planos (uno definido en el ecuador y otro definido en el primer meridiano). En este sentido una ubicación se define como la combinación entre un valor de latitud (-90:90) y un valor de longitud (-180:180).

####  Sistema de Coordenadas Proyectadas - SCP

Es un sistema de referencia de coordenadas bidimensional, se define normalmente mediante dos ejes. Ubicados en angulo recto uno respecto al otro, formando el denominado plano XY (como en un plano cartersiano).

#### Código EPSG

Todas las CRS cuentan con un código de la European Petroleum Survey Group (EPSG), el cual almacena los parámetros geodésicos con códigos estándar.

<!---------------->
## Sistemas de Coordenadas de Referencia (...)

Existen almenos 3 familias de Sistemas de Coordenadas Proyectadas, Plana (**a**),  Cónica (**b**) o Cilindrica (**c**). 

<div><img src="pics/projection_families.png" height=450></div>

Tomado de: [https://docs.qgis.org](https://docs.qgis.org/2.8/en/docs/gentle_gis_introduction/coordinate_reference_systems.html)

<!---------------->
## Sistemas de Coordenadas de Referencia (...)

Los CRS geográficos y los CRS proyectados están expresados en unidades diferentes:

![](pics/geographic_projected.png) 

Ver código de R [aquí](https://gitlab.com/Lectures-R/big-data-202201/Introduction-GIS/-/tree/main/public/clase_02/source/figure_csr.r)

<!---------------->
## Sistemas de Coordenadas de Referencia (...)

Dentro de los CRS geográficos/proyectadas pueden existir puntos de origen diferentes:

![](pics/origen.png)

Ver código de R [aquí](https://gitlab.com/Lectures-R/big-data-202201/Introduction-GIS/-/tree/main/public/clase_02/source/figure_csr.r)

<!------------------>
<!--- R-espacial --->
<!------------------>
# [3.] R-espacial
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

[Aquí](https://github.com/r-spatial) puede acceder a los repositorios de GitHub con las librerías de [r-spatial](https://github.com/r-spatial).

<!---------------->
## Popularidad de R-espacial
<div><img src="pics/download_packages.png" height=500></div>

Ver código de R [aquí](https://gitlab.com/Lectures-R/big-data-202201/Introduction-GIS/-/tree/main/public/clase_02/source/figure_spatial_pkgs.R)

<!---------------->
## Librería sf

Fue escrita por [Edzer Pebesma](https://github.com/edzer) y [Roger Bivand](https://github.com/rsbivand) quienes también la librería `sp` (la versión anterior de `sf`).  

#### ¿Por qué cambiar a `sf` si `sp` ya ha sido probado y probado? 

• Lectura y escritura rápida de datos.

• Rendimiento de trazado mejorado.

• Los objetos sf pueden tratarse como dataframes en la mayoría de las operaciones.

• Las funciones de sf se pueden combinar con el operador %>% y trabaja bastante bien con las funciones de la librería `tidyverse`.

• Los nombres de las funciones sf son relativamente consistentes e intuitivos (todos comienzan con st_).

<!---------------->
## Librería sf (...)

```{r,eval=T,echo=T}
vignette(package = "sf") # Ver viñetas disponibles
```
```{r,eval=F,echo=T}
Vignettes in package ‘sf’:

sf1                      1. Simple Features for R (source, html)
sf2                      2. Reading, Writing and Converting Simple
                         Features (source, html)
sf3                      3. Manipulating Simple Feature Geometries
                         (source, html)
sf4                      4. Manipulating Simple Features (source, html)
sf5                      5. Plotting Simple Features (source, html)
sf6                      6. Miscellaneous (source, html)
sf7                      7. Spherical geometry in sf using s2geometry
                         (source, html)
```

<!---------------->
## ¿Qué es un simple feature?

Un simple feature es una cosa u objeto en el mundo real, como una calle o un edificio. un `sf` posee una geometría (describe lugar de la Tierra en el que se encuentra) y unos atributos (describe otras propiedades del objeto). La geometría de un árbol puede ser el un circulo con el diametro de su copa o un punto que indica su centro. Las propiedades pueden incluir su altura, color, diámetro, etc.

<div><img src="https://docs.qgis.org/2.14/es/_images/feature_at_glance.png" height=380></div>
Tomado de: [https://docs.qgis.org](https://docs.qgis.org)   

<!---------------->
## Tipos de geometrías para `sf`
![](pics/type_geometry.png)

<!---------------->
## Objeto sf en R
```{r,eval=F}
class(sf_objetc)

"sf"         "data.frame"
```
<div><img src="pics/sf_console.png"></div>

<!--------------------->
<!--- Checklist --->
<!--------------------->
# Gracias
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Para seguir leyendo

* Lovelace, R., Nowosad, J., & Muenchow, J. (2019). **Geocomputation with R.** [[Ver aquí]](https://geocompr.robinlovelace.net)

  + Cap. 2: Geographic data in R
  + Cap. 6: Reprojecting geographic data
  + Cap. 8: Making maps with R

* Bivand, R. S., Pebesma, E. J., Gómez-Rubio, V., & Pebesma, E. J. (2013). **Applied spatial data analysis with R.** [[Ver aquí]](https://csgillespie.github.io/efficientR/)

  + Cap. 4: Spatial Data Import and Export
  
<!--------------------->
## Hoy vimos...

☑ Datos geográficos

☑ Sistemas de Referencia de Coordenadas - CRS

☑ R-espacial

<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.3em ; color:#CC0000 ; font-weight:bolder ; text-align:left}
.reveal section h3 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section h4 {font-size:0.9em ; color:#CC0000 ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section href {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:0.8em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {display: inline-block; border: 0px; background-color: #FFFFFF; align="center"}
</style>

